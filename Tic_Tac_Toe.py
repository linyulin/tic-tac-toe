from turtle import *
import math
speed(0)
# Square

def square(size, sq_color):
    # Copy your square function from Exercise 1 here
    color(sq_color)
    begin_fill()
    forward(size)
    right(90)
    forward(size)
    right(90)
    forward(size)
    right(90)
    forward(size)
    end_fill()
    right(90)

def next_color(current_color):
    # Copy your next_color function from Exercise 3 here
    if (current_color =="red"):
        return "black"
    else:
        return "red"

# For Exercise 4, Only modify this function
# This function draws a row of squares.  `number` is the number of squares across it should draw. Each square is of size sq_size.
# The colors start with sq_color, and then should be chosen by next_color()
def row(number, sq_size, sq_color):

    for i in range(number):

        square(sq_size , sq_color)
        sq_color = next_color(sq_color)
        forward(sq_size)

def draw_O(o_size):
    pensize(5)
    color("white")
    penup()
    right(90)
    pendown()
    circle(o_size / 2 )
    left(90)

def draw_X(x_size):
    penup()
    left(90)
    forward(50)
    right(90)
    pendown()
    pensize(5)
    color("white")
    right(45)
    forward(x_size * math.sqrt(2))
    left(135)
    penup()
    forward(x_size)
    pendown()
    left(135)
    forward(x_size * math.sqrt(2))
    left(135)

def write_axise():
    penup()
    color("black")
    goto(-160,220)
    pendown()
    write("C1", font=(30))

    penup()
    goto(-60,220)
    pendown()
    write("C2", font=(30))

    penup()
    goto(40,220)
    pendown()
    write("C3", font=(30))

    penup()
    goto(-230,150)
    pendown()
    write("R1", font=(30))

    penup()
    goto(-230,50)
    pendown()
    write("R2", font=(30))

    penup()
    goto(-230,-50)
    pendown()
    write("R3", font=(30))




def R1_C1():
    penup()
    goto(-200,150)
    pendown()

def R1_C2():
    penup()
    goto(-100,150)
    pendown()

def R1_C3():
    penup()
    goto(0,150)
    pendown()

def R2_C1():
    penup()
    goto(-200,50)
    pendown()

def R2_C2():
    penup()
    goto(-100,50)
    pendown()

def R2_C3():
    penup()
    goto(0,50)
    pendown()

def R3_C1():
    penup()
    goto(-200,-50)
    pendown()

def R3_C2():
    penup()
    goto(-100,-50)
    pendown()

def R3_C3():
    penup()
    goto(0,-50)
    pendown()
#### Do not modify anything below this line before Exercise 7
# Draw board

penup()
goto(-200, 200)
pendown()
square(300, "black")
first_color = "red"
for i in range(3):
    row(3, 300/3, first_color)
    first_color = next_color(first_color)

    penup()
    back(300)
    right(90)
    forward(300/3)
    left(90)
    pendown()

write_axise()

penup()
goto(-200, 200)
pendown()


def next_player(current_player):
    if( current_player == "O"):
        return "X"
    else:
        return "O"


current_player = "O"

for i in range(9):

    axise= input("Where would you like to place the "+ current_player+" ? (R,C)")
    if (current_player == "O"):
        if (axise == "R1,C1"):
            R1_C1()
        if (axise == "R1,C2"):
            R1_C2()
        if (axise == "R1,C3"):
            R1_C3()
        if (axise == "R2,C1"):
            R2_C1()
        if (axise == "R2,C2"):
            R2_C2()
        if (axise == "R2,C3"):
            R2_C3()
        if (axise == "R3,C1"):
            R3_C1()
        if (axise == "R3,C2"):
            R3_C2()
        if (axise == "R3,C3"):
            R3_C3()

        draw_O(100)

    if (current_player == "X"):
        if (axise == "R1,C1"):
            R1_C1()
        if (axise == "R1,C2"):
            R1_C2()
        if (axise == "R1,C3"):
            R1_C3()
        if (axise == "R2,C1"):
            R2_C1()
        if (axise == "R2,C2"):
            R2_C2()
        if (axise == "R2,C3"):
            R2_C3()
        if (axise == "R3,C1"):
            R3_C1()
        if (axise == "R3,C2"):
            R3_C2()
        if (axise == "R3,C3"):
            R3_C3()

        draw_X(100)
    current_player = next_player(current_player)






done()
